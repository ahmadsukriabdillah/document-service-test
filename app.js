/**
 * INCLUDE LIBRARY
 */
require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const app = express();
const cors = require("cors");
const helmet = require("helmet");
const service = require("./src/services");
const controller = require("./src/controllers");
const utils = require("./src/utils");
const mongoose = require("mongoose");

/**
 *
 * KONFIGURASI
 */

app.use(cors());


let DB = process.env.DB;

mongoose
  .connect(DB, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("mongoDB connected Successfully"))
  .catch((err) => console.log(err));

app.use(
  morgan('[:date[web]] ":method :url" :status :response-time ms', {
    stream: utils.logger.stream
  })
);

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(helmet());
app.disable("x-powered-by");

app.validate = require("express-validation");
app.Joi = require("joi");

require("./src/routes")(app, service, utils, controller);

app.use((err, req, res, next) => {
  utils.error_handler(err, res);
});

module.exports = app;
