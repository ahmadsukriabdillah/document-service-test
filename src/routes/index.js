const WHITELIST = [
  "/",
  "/api/auth/login"
];
var jwt = require("express-jwt");
var path = require("path");
const {Document} = require("./../models")

module.exports = (app, service, utils, controller) => {
  app.use(
    jwt({
      secret: utils.config.secret
    }).unless({ path: WHITELIST })
  );
  app.get("/api", async (req, res) => {
    let folder = await Document.find({}).select("id name type is_public owner_id timestamp share company_id");
    res.json({
        error: false,
        data: folder
    });
  })
  app.use("/api/folder", controller.folder_controller({ app, service, utils }))
  app.use("/api/document", controller.document_controller({ app, service, utils }))
  app.use("/api/auth", controller.user_controller({ app, service, utils }));
};
