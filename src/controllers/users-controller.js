const { Router } = require("express");
const index = Router();
const { Users } = require("./../models");


module.exports = ({
  app: { validate, Joi },
  service: { UserService },
  utils: { BuildResponse }
}) => {
  index.post(
    "/login",
    [
      validate({
        body: {
          username: Joi.string()
            .max(50),
          password: Joi.string()
        }
      })
    ],
    async (req, res, next) => {
      try {
        let data = await UserService.authentication_basic(req);
        BuildResponse(req, res, data);
      } catch (err) {
        next(err);
      }
    }
  );
  return index;
};
