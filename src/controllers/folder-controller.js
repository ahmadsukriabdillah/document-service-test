const { Router } = require("express");
const index = Router();
const { Document } = require("../models")
module.exports = ({
  app: { validate, Joi },
  service: { UserService },
  utils: { BuildResponse }
}) => {

    index.post("/",
    [
        validate({
            body: {
                id: Joi.string().required(),
                name: Joi.string().required(),
                timestamp: Joi.number().required(),
            }
        })
    ],async (req, res, next) => {
        let folder = await Document.findOne({id: req.body.id}).exec();
        if(folder){
            await Document.updateOne({id: req.body.id}, {
                name: req.body.name,
                timestamp: req.body.timestamp,
            });
            res.json({
                error: false,
                message: "folder updated",
                data: {
                    id: req.body.id,
                    name: req.body.name,
                    timestamp: req.body.timestamp,
                }
            });
        }else{
            console.log(req.user);
            let payload = {
                id: req.body.id,
                name: req.body.name,
                folder_id: req.body.folder_id || "",
                type: "folder",
                share: req.body.share || [],
                company_id: req.user.company_id,
                owner_id: req.user.user_id,
                timestamp: req.body.timestamp
            }

            Document.create(payload, function (err, small) {
                if (err) return handleError(err);
                // saved!
                res.json({
                    error: false,
                    message: "folder created",
                    data: small
                });
            });
        }        
    });

    index.delete("/", [
        validate({
            body: {
                id: Joi.string().required()
            }
        })
    ], async (req, res, next) => {
        Document.findOneAndDelete({id: req.body.id},null, (err, doc) => {
            res.json({
                error: false,
                message: "Success delete folder"
            });
        });
    });
    index.get("/:folder_id", async (req, res) => {
        let folder = await Document.find({folder_id: req.params.folder_id});
        res.json({
            error: false,
            data: folder
        });
    });
    return index;
};