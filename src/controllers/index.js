const user_controller = require("./users-controller");
const folder_controller = require("./folder-controller");
const document_controller = require("./document-controller");

module.exports = {
  user_controller,
  folder_controller,
  document_controller
};
