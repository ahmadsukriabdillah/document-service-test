const {
  db,
  config,
  UnauthorizerException,
} = require("./../utils");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { Users } = require("./../models");

const authentication_basic = async ({ body: { username, password } }) => {
  let user = await Users.findOne({username: username}).lean()
  if (!user) {
    throw new UnauthorizerException(
      "Invalid Username / Password.",
      "Username / password tidak benar. user tidak di temukan"
    );
  } else {
    const match = await bcrypt.compare(password, user.password || "");
    // if (match) {
      delete user.password;
      let token = jwt.sign({...user}, config.secret, {
        expiresIn: "365 days"
      });
      return {
        status_code: 200,
        data: {
          ...user,
          access_token: token
        }
      };
    // } else {
    //   throw new UnauthorizerException(
    //     "Invalid Username / Password.",
    //     "Username / password tidak benar."
    //   );
    // }
  }
};


module.exports = {
  authentication_basic
};
