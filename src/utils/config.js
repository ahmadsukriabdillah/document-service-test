module.exports = {
  secret: process.env.JWT_SECRET_PASSWORD,
  salt_round: process.env.SALT_ROUND
};
