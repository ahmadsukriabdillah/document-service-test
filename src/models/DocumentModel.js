const mongoose = require("mongoose");

const DocumentModel = new mongoose.Schema(
  {
    id: String,
    name: String,
    type: String,
    folder_id: String,
    is_public: Boolean,
    content: mongoose.Schema.Types.Mixed,
    owner_id: Number,
    share: [Number],
    timestamp: Number,
    company_id: Number
  }
);

const Document = mongoose.model("Document", DocumentModel);
module.exports = Document;