const mongoose = require("mongoose");

const UsersModel = new mongoose.Schema(
  {
    user_id: Number,
    username: String,
    password: String,
    company_id: Number,
    email: String
  }
);

const Users = mongoose.model("Users", UsersModel);
module.exports = Users;