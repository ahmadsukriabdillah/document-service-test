const Document = require("./DocumentModel");
const Users = require("./UserModel");

module.exports = {
    Document,
    Users
}